#Linguaggi, compilatori e modelli computazionali

**Teoremi: enunciati e dimostrazioni di teoria presenti nelle slide**

_A Bravetflix original series_


_**Indice**_

[toc]


#Linguaggi regolari

##Teorema 2.11

**Enunciato**: sia $D$ il DFA ottenuto da un NFA $N$ con la costruzione a sottoinsiemi. Allora $L(D) = L(N)$.

##Teorema 2.12

**Enunciato**: Un linguaggio $L$ è accettato da un DFA se e solo se $L$ è accettato da un DFA.

_Lemma_: il numero di stati del DFA equivalente ad un NFA con $n$ stati è, nel caso peggiore, pari a $2^n$ stati.

##Teorema 2.22

**Enunciato**: un linguaggio $L$ è accettato da un $ε$-NFA $E$ se e solo se $L$ è accettato da un DFA.

##Teorema 3.7

**Enunciato**: per ogni espressione regolare $R$ possiamo costruire un $\epsilon$-NFA $A$ (con un unico stato finale, diverso da quello iniziale), tale che $L(A) = L(R)$.

**Dimostrazione**: per induzione strutturale.

- _Base_: automa per $\epsilon$, $∅$, e $a$.

  ![](imgs/t3-7-a1.png)
  
- _Induzione_: automa per $R + S$, $RS$ e $R^*$

  ![](imgs/t3-7-b.png)
  
##Teorema 4.1 _(Pumping lemma per linguaggi regolari)_
  
_Se esiste una stringa abbastanza lunga per causare un ciclo in un DFA per il linguaggio, allora possiamo “ripetere” il ciclo e scoprire una infinita’ di stringhe che appartengono al linguaggio._

**Enunciato**: sia $L$ un linguaggio regolare. Allora $\exists n \geq 1$ che soddisfa:
ogni $w \in L : |w| \geq n$ è scomponibile in tre stringhe $w = xyz$ tali che

1. $y \neq \epsilon$2. $|xy|≤n$3. $\forall k≥0, xy^kz \in L$

**Dimostrazione**: supponiamo che $L$ sia regolare. Allora $L$ è riconosciuto da un DFA $A$. Chiamiamo **$n$** il **numero degli stati** di $A$.
Sia $w=a_1a_2...a_m \in L, m \geq n$. Sia $p_i = \delta(q_0,a_1a_2...a_i)$, dove $q_0$ è lo stato inziale di $A$. $⇒∃ i < j \leq n : p_i = p_j$.

Ora $w = xyz$, dove
1. $x = a_1a_2...a_i$2. $y = a_i+_1a_i+_2...a_j$
3. $z = a_j+_1a_j+_2...a_m$

![](imgs/pumpinglemma-schema.png)

Quindi anche $xy^kz \in L$, per ogni $k ≥ 0$.

##Teorema 4.4

**Enunciato**: per ogni coppia di linguaggi regolari $L$ e $M$, $L \cup M$ è regolare.

**Dimostrazione**: sia $L = L(E)$ e $M = L(F)$. Allora $L(E+F) = L \cup M$ per definizione.

##Teorema 4.5

**Enunciato**: se $L$ è un linguaggio regolare su $\Sigma$, allora anche $\ overline{L} = \Sigma* \backslash L$ è regolare.

**Dimostrazione**: sia $L$ riconosciuto da un DFA $A = (Q,\Sigma,\delta,q_0,F)$. Sia $B = (Q,\Sigma,\delta,q_0,Q \backslash F)$. Allora $L(B)=\overline{L}$.

##Teorema 4.8

**Enunciato**: se $L$ e $M$ sono regolari, allora anche $L \cap M$ è regolare.
**Dimostrazione**: per la legge di DeMorgan, $L \cap M = L \cup M$ . Sappiamo già che i linguaggi regolari sono chiusi rispetto al complemento e all'unione.

**Dimostrazione alternativa**: sia $L$ il linguaggio di $A_L = (Q_L, \Sigma, \delta_L,q_L,F_L)$ e $M$ il linguaggio di $A_M = (Q_M, \Sigma, \delta_M, q_M, F_M)$. Assumiamo senza perdita di generalità che entrambi gli automi siano deterministici. Costruiremo un automa che simula $A_L$ e $A_M$ in parallelo, e accetta se e solo se sia $A_L$ che $A_M$ accettano.

Se $A_L$ va dallo stato $p$ allo stato $s$ leggendo $a$, e $A_M$ va dallo stato $q$ allo stato $t$ leggendo $a$, allora $A_{L \cap M}$ andra’ dallo stato $(p,q)$ allo stato $(s,t)$ leggendo $a$.

![](imgs/t4-8-1.png)

Formalmente, $A_{L \cap M} = (Q_L \times Q_M, \Sigma, \delta _{L \cap M}, (q_L, q_M), F_L \times F_M)$

dove $ \delta _{L \cap M} ((p,q),a) = (\delta_L(p,a), \delta_M (q,a))$.

Si può mostrare per induzione su $|w|$ che $\bar{\delta}_{L \cap M} ((q_L, q_M), w) = ( \bar{ \delta } _L (q_L, w), \bar{\delta} _M(q_M,w))$

##Teorema 4.10

**Enunciato**: se $L$ e $M$ sono linguaggi regolari, allora anche $L \backslash M$ è regolare.

**Dimostrazione**: osserviamo che $L \backslash M = L \cap M$ . Sappiamo già che i linguaggi regolari sono chiusi rispetto al complemento e all'intersezione.

##Teorema 4.11

**Enunciato**: se $L$ è un linguaggio regolare, allora anche $L^R$ (le stringhe $w^R$, cioè $w$ lette al contrario, con $w \in L$) è regolare.

**Dimostrazione**: sia $L$ riconosciuto da un FA $A$. Modifichiamo $A$ per renderlo un FA per $L^R$:
1. Giriamo tutti gli archi.2. Rendiamo il vecchio stato iniziale l’unico stato finale.3. Creiamo un nuovo stato iniziale $p_0$, con $\Sigma(p_0, \epsilon) = F$ (i vecchi stati finali).

##Teorema 4.20

**Enunciato**: se $p$ e $q$ non sono distinguibili dall'algoritmo, allora $p \equiv q$.

**Dimostrazione**: suppiniamo per assurdo che esista una coppia "sbagliata" $\{p,q\}$ tale che

1. $\exists w : \overline {\delta} (p,w) \in F, \overline {\delta} (q,w) \notin F$, o viceversa
2. L'algoritmo non distingue tra $p$ e $q$.

Assumiamo senza perdita di generalità che $\{p,q\}$ sia la coppia sbagliata con la stringa più corta $w = a_1a_2...a_n$ che la identifica come tale.

Allora $w \neq \epsilon$ perchè altrimenti l'algoritmo distinguerebbe $p$ da $q$ (caso base). Quindi $n \geq 1$.

*In parole povere, consideriamo gli stati $r = \delta(p, a_1)$ e $s = \delta(q, a_1)$. Allora $\{r, s\}$ non può essere una coppia sbagliata perchè $\{r,s\}$ sarebbe identificata da una stringa più corta di $w$. Quindi, l'algoritmo deve aver scoperto che $r$ e $s$ sono distinguibili.Ma allora l'algoritmo distinguerebbe $p$ da $q$ nella parte induttiva successiva alla scoperta della distinzione fra $r$ ed $s$. Quindi non ci sono coppie "sbagliate" e il teorema è vero.*

##Teorema 4.23

**Enunciato**: se $p \equiv q$ e $q \equiv r$, allora $p \equiv r$.
**Dimostrazione**: supponiamo per assurdo che $p \not\equiv r$. Allora $\exists w$ tale che $ \overline {\delta} (p,w) \in F$ e $\bar{\delta}(r,w) \notin F$ o viceversa.

Lo stato $ \overline {\delta}(r,w)$:

- è di accettazione, allora $q \not\equiv r$
- non è di accettazione, allora $p \not\equiv q$

Il caso contrario può essere provato simmetricamente. Quindi deve essere $p \equiv r$


#Linguaggi liberi dal contesto

##Teorema 5.29

**Enunciato**: data una CFG $G$, una stringa terminale $w$ ha due distinti alberi sintattici se e solo se $w$ ha due distinte derivazioni a sinistra dal simbolo iniziale.

##Teorema 6.9

**Enunciato**: se $L = N(P_N)$ per un PDA $P_N = (Q,\Sigma,\Gamma,\delta_N,q_0,Z_0)$, allora $\exists$ PDA $P_F$, tale che $L = L(P_F)$.

**Dimostrazione**: sia $P_F = (Q \cup \{p_0,p_f\}, \Sigma , \Gamma \cup \{X_0\},\delta_F,p_0,X_0,\{pf\})$, dove $\delta_F(p_0, \epsilon ,X_0) = \{(q_0,Z_0,X_0)\}$, e per ogni $q \in Q$, $a \in \Sigma \cup \{\epsilon\},Y \in \Gamma : \delta_F(q,a,Y) = \delta_N(q,a,Y)$, e inoltre $(p_f, \epsilon) \in \delta_F(q,\epsilon,X_0)$.

Dobbiamo mostrare che $L(P_F) = N(P_N)$.

- _**direzione $\supset$**_: sia $w \in N(P_N)$. Allora $(q_0, w, Z_0) \vdash_{N}^* (q, \epsilon, \epsilon)$, per qualche $q$. Dal teorema 6.5 ( 😅 ) abbiamo $(q_0, w, Z_0X_0) \vdash_{N}^* (q, \epsilon, X_0)$.
Dato che $\delta_N \subset \delta_F$, abbiamo $(q_0, w, Z_0X_0) \vdash_{F}^* (q, \epsilon, X_0)$. Concludiamo che $(p_0, w, X_0) \vdash_{F} (q_0, w, Z_0X_0) \vdash_{F}^* (q, \epsilon, X_0) \vdash_{F} (p_f, \epsilon, \epsilon)$.
- _**direzione $\subset$**_: basta esaminare il diagramma.

##Teorema 6.11

**Enunciato**: Sia $L=L(P_F)$, per un PDA $P_F =(Q,\Sigma,\Gamma,\deltaF,q_0,Z_0,F)$. Allora $\exists$ PDA $P_N$, tale che $L = N(P_N)$.







#Argomenti dubbi
(nel senso che non so se li mette)

- Definizione DFA
- Definizione NFA
- Equivalenza DFA=NFA
- FA con transizioni epsilon
- ECLOSE
- Equivalenza DFA = ε-NFA
- REGEX e operazioni possibili
- Equivalenza FA=REGEX
- **Teorema 3.4 (pg. 35)** da DFA a REGEX
- Leggi algebriche per i linguaggi
- Perchè non si può migliorare il DFA minimizzato
- Definizione CFG
- Ambiguità inerente







