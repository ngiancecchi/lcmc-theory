#Linguaggi, compilatori e modelli computazionali

**Domande esami precedenti**


###Riportare enunciato e dimostrazione del Pumping Lemma per linguaggi regolari.
  
_Se esiste una stringa abbastanza lunga per causare un ciclo in un DFA per il linguaggio, allora possiamo “ripetere” il ciclo e scoprire una infinita’ di stringhe che appartengono al linguaggio._

**Enunciato**: sia $L$ un linguaggio regolare. Allora $\exists n \geq 1$ che soddisfa:
ogni $w \in L : |w| \geq n$ è scomponibile in tre stringhe $w = xyz$ tali che

1. $y \neq \epsilon$2. $|xy|≤n$3. $\forall k≥0, xy^kz \in L$

**Dimostrazione**: supponiamo che $L$ sia regolare. Allora $L$ è riconosciuto da un DFA $A$. Chiamiamo **$n$** il **numero degli stati** di $A$.
Sia $w=a_1a_2...a_m \in L, m \geq n$. 

Sia $p_i = \delta(q_0,a_1a_2...a_i)$, dove $q_0$ è lo stato inziale di $A$. 

$⇒∃ i < j \leq n : p_i = p_j$.

Ora $w = xyz$, dove
1. $x = a_1a_2...a_i$2. $y = a_{i+1}a_{i+2}...a_j$
3. $z = a_{j+1}a_{j+2}...a_m$

![](imgs/pumpinglemma-schema.png)

Quindi anche $xy^kz \in L$, per ogni $k ≥ 0$.

###Riportare enunciato e dimostrazione del Pumping Lemma per linguaggi liberi.

_per una stringa sufficientemente lunga e’ sempre possibile trovare due pezzi distinti da ripetere “in tandem”: ripetendoli lo stesso numero di volte “i”, otteniamo, per ogni “i”, una nuova stringa appartenente al linguaggio_

**Enunciato**: sia $L$ un CFL. Allora $\exists n \geq 1$ che soddisfa:
ogni $z \in L : |z| \geq n$ è scomponibile in 5 stringhe $z = uvwxy$ tali che:
1. $|vwx|\leq n$2. $|vx|>0$3. $\forall i \geq 0, uv^iwx^iy \in L$

**Dimostrazione**: si consideri una grammatica per $L \backslash \{\epsilon\}$ in CNF. Assumiamo che la grammatica abbia $m$ variabili. 

Sia $n = 2^m$. Sia $z \in L$ una qualsiasi stringa tale che $|z| \geq n = 2^m$. Si ha che ogni parse tree di $z$ contiene un cammino di lunghezza $> m+1$.

**Lemma**. Se tutti i cammini del parse tree hanno lunghezza $\leq m$,, allora la stringa generata ha lunghezza $\leq 2^{m-1}$. Consideriamo un cammino $A_0A_1...A_ka$ di lunghezza massima $\geq m+1$. Esistono $i \neq j$ tali che $A_i = A_j$ (con $i,j$ ultime $m+1$ variabili del cammino).

**Osservazioni**. Si osserva che:

- $|vwx| \leq n$: l'albero in $A_i$ ha altezza $\leq m+1$, quindi la stringa corrispondente ha lunghezza $\leq 2^m = n$
- $|vx| > 0$: $v$ e $x$ non possono essere entrambe vuote in quanto $A_i$ essendo in CNF genera due variabili non annullabili- $\forall i \geq 0, uv^iwx^iy \in L$: il parse tree ottenuto ripetendo un numero arbitrario di volte (anche 0) la parte di albero in $A_i$ meno l'albero in $A_j$ continua ad essere un parse tree corretto.

###_Trasformazioni_


**RE -> e-NFA**

**DFA->RE**

**e-NFA->DFA**

**DFA->e-NFA** (sim.)

**MANCA! NFA->DFA**


###RE->e-NFA - Descrivere formalmente come si ottiene un ε-NFA a partire da una espressione regolare.

**Enunciato**: per ogni espressione regolare $R$ possiamo costruire un $\epsilon$-NFA $A$ (con un unico stato finale, diverso da quello iniziale), tale che $L(A) = L(R)$.

**Dimostrazione**: per induzione strutturale.

- _Base_: automa per $\epsilon$, $∅$, e $a$.

  ![](imgs/t3-7-a1.png)
  
- _Induzione_: automa per $R + S$, $RS$ e $R^*$

  ![](imgs/t3-7-b.png)

###e-NFA->DFA - Descrivere formalmente come si ottiene un DFA equivalente a partire da un ε-NFA.

Dato un $\epsilon$-NFA $E$, possiamo trovare un DFA $D$ che accetta lo stesso linguaggio di $E$ (procedimento simile a NFA->DFA).

Sia $E = (Q_E, \Sigma, \delta_E, q_0, F_E)$. Il DFA equivalente $D = (Q_D, \Sigma, \delta_D, q_D, F_D)$ è definito come segue:

1. $Q_d$ = insieme dei sottoinsiemi di $Q_E$. Un insieme di stati $S$ è $\epsilon$-chiuso se ogni $\epsilon$-transizione uscente da uno stato di $S$ porta ad uno stato di $S$. $\O$ (ins. vuoto) è un insieme $\epsilon$-chiuso.
2. $q_D = ECLOSE(q_0)$
3. $F_d$ = insiemi di stati che contengono almeno uno stato accettante $E$.
4. $\forall a \in \Sigma$ e $\forall$ insieme S in $Q_d$, $\delta_D(S,a) si calcola come segue:
	- Sia $S = \{p_1,p_2,...,p_k\}$
	- Si calcoli $\cup_{i=1}^k \delta_E(p_i, a)$. Sia $\{r_1,r_2...r_m\}$ il risultato.
	- Allora $\delta_D(S,a) = ECLOSE(\{r_1,r_2,...,r_m\})$.

###DFA->e-NFA




###DFA->RE - Descrivere come si ottiene una espressione regolare a partire da un DFA con uno (a libera scelta) dei due metodi visti a lezione (e definiti nel libro di testo).

**Enunciato**: Per ogni DFA $A = (Q, \Sigma, \delta, q_0, F )$ esiste una espressione regolare $R$, tale che $L(R) = L(A)$.

**Procedimento**: 

1. Trasformiamo l’automa etichettando gli archi con espressioni regolari di simboli.
2. Eliminiamo lo stato $s$. (esempio di eliminazione di uno stato).
3. Per lo stato accettante $q$ eliminiamo dall'automa originale tutti gli stati eccetto $q_0$ e $q$.
4. Per ogni $q \in F$ saremo rimasti con $A_q$ della forma che corrisponde all’espressione regolare $E_q = (R + SU∗T)∗SU∗$ o con $A_q$ della forma che corrisponde all’espressione regolare $Eq = R∗$.
5. L'espressione finale è $ \bigoplus Eq, q \in F$

###NFA->DFA

Dato un NFA $N = (Q_N, \Sigma, \delta_N, q_0, F_N)$ è possibile ricavare un DFA $D = (Q_D, \Sigma, \delta_D, q_0, F_D)$ tramite costruzione per sottoinsiemi. Per ogni sottoinsieme $S \subset Q_n$ e per ogni simbolo di input $a$ in $\Sigma$, $\delta_D(S,a) = _p\cup_{in S} \delta_N(p,a)$.

_Creare tabella di transizione con gli stati che possiedono più di una freccia in uscita. I risultati per ogni nuovo stato saranno la somma degli stati descritti in input. Ad esempio se ho $\{p\}$ che va in $\{p,q\}$ con input 0, allora lo stato $\{p,q\}$ sarà dato dalla somma degli stati $p$ e $q$. E' possibile associare a ciascun nuovo stato una lettera così da migliorare la leggibilità. E' possibile eliminare gli stati che non portano a nulla (nel nuovo DFA). Gli stati finali del DFA saranno tutti quelli in cui compariranno i terminali finali del NFA._

###CFG->PDA - Descrivere come si può ottenere un PDA equivalente ad una data CFG.

Data una grammatica $G$, costruiamo un PDA che simula una derivazione a sinistra ($\Rightarrow_{lm}^*$). Scriviamo le forme sentenziali come $xA\alpha$, dove $A$ è la variabile più a sinistra. Sia $xAa \Rightarrow_{lm} x\beta\alpha$. Questo corrisponde al PDA che ha prima consumato $x$ e ha $A\alpha$ sulla pila, e poi, leggendo $	\epsilon$, elimina $A$ e mette $\beta$ sulla pila.

Più formalmente, sia $y$ tale che $w = xy$. Allora il PDA va non deterministicamente dalla configurazione $(q,y,A\alpha)$ alla configurazione $(q,y,\beta\alpha)$.

Alla configurazione $(q,y,\beta\alpha)$ il PDA si comporta come prima, a meno che ci siano terminali nel prefisso di $\beta$. In questo caso, il PDA li elimina, se li legge nell'input. Se tutte le scommesse sono giuste, il PDA finisce l'input con la pila vuota.

**Quindi la trasformazione è la seguente. Sia $G = (V,T,Q,S)$ una CFG. Definiamo $P_G$ come $({q},T,V \cup T, \delta, q, S)$, dove $\delta(q,\epsilon,A)=\{(q,\beta) : A \rightarrow \beta \in Q\}$ per $A \in V$, e $\delta(q,a,a) = \{(q,\epsilon)\}$ per $a \in T$.**

###PDA->CFG - Descrivere come si può ottenere una CFG equivalente ad un dato PDA.

Sia $P = (Q, \Sigma, \Gamma, \delta, q_0, Z_0)$ un PDA. Definiamo $G = (V, \Sigma, R, S)$, dove:

- $V = \{[pXq]:\{p,q\} \subset Q, X \in \Gamma\} \cup \{S\}$
- $R = \{S \rightarrow [q_0Z_0p] : p \in Q\} \cup 

\{[qXr_k] \rightarrow a[rY_1r_1]...[r_{k-1}Y_kr_k] : 

a \in \Sigma \cup \{\epsilon\}, 

\{r_1...r_k\} \subset Q, 

(r, Y_1,...Y_k) \in \delta(q, a, X)\} $ 


###Fornire la definizione di PDA e la definizione formale: del linguaggio che esso riconosce per pila vuota e del linguaggio che esso, invece, riconosce per stato finale (definendo preliminarmente le descrizioni istantanee dell’automa e come esse si calcolano in base alla sua funzione di transizione).



###Dare la definizione formale di linguaggio ricorsivamente enumerabile. Successivamente dare, inoltre, la definizione di linguaggio ricorsivo.

- **Linguaggio ricorsivamente enumerabile**: un $L$ è RE se $L = L(M)$ per una TM $M$. 
- **Linguaggio ricorsivo**: è ricorsivo un linguaggio $L$ se $L = L(M)$ per una macchina di Turing tale che
	1. se $w$ è in $L$, allora $M$ accetta (e si arresta)
	2. se $w$ non è in $L$, allora $M$ si arresta pur non entrando in uno stato accettante.

_Un problema è decidibile se si tratta di un linguaggio ricorsivo, è indecidibile se non si tratta di un ricorsivo_


###Dare la definizione formale di linguaggio ricorsivamente enumerabile. Si consideri inoltre il problema di verificare se un dato linguaggio ricorsivamente enumerabile è vuoto oppure no. Dire se tale problema è decidibile oppure no, e giustificare la risposta.

**Definizione**: vedi domanda prima.



###Elencare e descrivere i tipi di parser bottom-up visti a lezione spiegando le differenze e mettendoli in ordine di potere espressivo (classe di grammatiche di cui riescono a fare il parsing).

###Data una CFG G elencare e definire formalmente le proprietà che G deve necessariamente avere per poter essere utilizzata in un parser top-down. Dire inoltre quali di esse possono essere ottenute tramite trasformazioni preliminari algoritmiche da effettuare su G.



###Riportare la definizione di linguaggio di diagonalizzazione $L_d$ e di linguaggio universale $L_u$ .

- **Linguaggio di diagonalizzazione** ($L_d$): insieme delle stringhe $w_i$ tali che $w_i$ non è in $L(M_i)$.
- **Linguaggio universale**: insieme delle stringhe binarie che codificano una coppia $(M, w)$, dove $M$ è una TM con alfabeto in input binario e $w$ è una stringa in $(0 + 1)*$ tale che $w$ sia in $L(M)$.


###Riportare la definizione di linguaggio di diagonalizzazione $L_d$ e la relativa dimostrazione del fatto che tale linguaggio non è ricorsivamente enumerabile.

**Definizione**: vedi domanda prima

**Enunciato**. $L_d$ non è un linguaggio ricorsivamente enumerabile, cioè non esiste alcuna macchina di Turing che accetta $L_d$.

**Dimostrazione**. Supponiamo $L_d = L(M)$ per una TM $M$. Poichè $L_d$ è un linguaggio sull'alfabeto $\{0,1\}$, $M$ rientra nell'elenco di macchine di Turing che abbiamo costruito, dato che questo elenco include tutte le TM con alfabeto di input $\{0,1\}$. Di conseguenza, esiste almeno un codice per $M$, poniamo $i$ ossia $M = M_i$. 

$w_i$ è in $L_d$?

- se $w_i$ è in $L_d$, allora $M_i$ accetta $w_i$. Per definizione di $L_d$, $w_i$ non è in $L_d$ poichè esso contiene solo le stringhe $w_j$ tali che $M_j$ non accetta $w_j$.
- se $w_i$ non è in $L_d$, allora $M_i$ non accetta $w_i$. Per definizione di $L_d$, $w_i$ è in $L_d$.

$w_i$ non può essere contemporaneamente dentro e fuori $L_d$, quindi abbiamo una contraddizione all'ipotesi che $M$ esista $\Rightarrow$ $L_d$ non è ric.enum.

###Descrivere in dettaglio l’algoritmo CYK dopo aver spiegato a cosa serve e come (e se) lo si può applicare ad una qualsiasi CFG .

 

 
