###Linguaggi, compilatori e modelli computazionali###
#Model checking#

####ToC####
* logiche temporali
* automi e logiche temporali
* modelli per sistemi concorrenti
	* process algebra
	* reti di Petri
	
##Logiche temporali##
Permettono di esprimere proprietà temporali, vere in alcuni mondi, false in altre.
Tali mondi corrispondono a diversi momenti temporali.

**Linear Temporal Logic (LTL)**: estende la logica proporzionale con operaotri per specificare proprietà su sequenze linear time di mondi:

####Sintassi LTL####

Dati **x** e **y**,

* **p |** proposizione atomica
* **⏉ |** vero
* **⏊ |** falso
* **⏋x |** complemento di x
* **x ^ y |** congiunzione di x con y
* **x v y |** disgiunzione di x con y
* **() x |** x è vero al *prossimo* momento (*next*) - anche X
	* Es. (sad ^ ⏋rich) => ()sad
* **[] x |** x è vero in *tutti* i momenti futuri (*always*) - anche G
	* Es. lottery-win => []rich
* **<> x |** x è vero in *un qualche* momento futuro (*sometime*) - anche F
	* Es. send => <>receive 
* **x *u* y |** x è vero *fino a quando* y è vero
	* Es. start_lecture => talk *u* end_lecture

*Esempio:* **\[] (( ⏋passport v ⏋ticket) => ()⏋board_flight )**

#####Operatori minimali#####
* *always* e *sometime* sono operatori duali: ⏋[]x = <>⏋x
* *sometime* può essere espresso usando *until*: <>x = ⏉ *u* x
	* => tutti gli operatori possono essere espressi usando solo *next* e *until* 
	
#####Matching con operatori classici#####
*sometime* distribuisce su or, *always* su and. Il complemento si relaziona con il not.
	
####Sequenze di mondi###
Si considerino formule LTL che utilizzino proporzioni p appartenenti a P. Un modello π è una sqeuenza infinita π<sub>1</sub>, π<sub>2</sub>... di mondi dove **π<sub>i</sub>** è una funzione **π<sub>i</sub> : P -> {T, F}**.

####Semantica LTL####
WTF?..

![](imgs/ltlpredlogic.png)

###Proprietà tipiche dei sistemi software###

* **safety**: non si raggiungono mai stati con errori.
	* *"qualcosa di cattivo non accadrà mai"*: **[] ⏋** (reactor_temp > 1000)
* **liveness**: prima o poi si eseguirà una certa azione.
	* *"qualcosa di buono accadrà"*: **<>**rich, **<>**(x > 5),... 
* **fairness**: se si richiede una cosa infinite volte, questa verrà eseguita infinite volte.
	* **[]<>**ready => **[]<>**run 
	
###Labeled Transition Systems###
**LTS** è sostanzialmente un NFA senza stati di accettazione F. Una traccia di LTS è una sequenza di etichette I1, I2, I3,.... In: ricordano le stringhe degli NFA, ma in questo caso interessano le **massimali**, ovvero quelle di lunghezza finita o che terminano in uno stato di blocco (senza uscita). *Def. formale pg. 27*

####Esprimere proprietà LTS con formule di logica temporale####
In ogni stato attraversato lungo una traccia vale una sola proposizione coincidente con l'etichetta della transizione successiva (I1, I2, I3... {I1}, {I2}, {I3}...). Un LTS soddisfa una formula di LTL se tutte le sue tracce massimali soddisfano tale formula. *Def. formale pg. 29*

**Esempio con mutua esclusione, pg 30-42**

##Sistemi concorrenti##
I sistemi modellati da questi automi sono sistemi concorrenti.

###Process Algebra###
Modo naturale per rappresentare sistemi di questo tipo, ne esistono di diversi tipi (CCS, ACP, CSP, pi-calculus, etc...). Trattiamo FSP (finite state processes) usato dal tool LTSA (labelled transition system analyzer).
Ogni processo è rappresentato come un LTS: si danno i nomi agli stati e per ogni stato si descrivono le transizioni che lo stato può fare.

*Esempi:*

* SWITCH = (on->off->SWITCH)
* TRAFFICLIGHT = (red->orange->green->orange->TRAFFICLIGHT)
* Lancio della moneta: COIN = (toss->HEADS|toss->TAILS), HEADS = (heads->COIN), TAILS = (tails->COIN)

####Definizioni####

* **concorrenza (interleaving)**: esecuzione 'logicamente' parallela di processi (LTS) come in un sistema multitasking
* **sincronizzazione**: esecuzione 'fisicamente' contemporanea di azioni

*Esempi: composizione parallela tramite...*

* interleaving: ITCH = (scratch->STOP). CONVERSE = (think->talk->STOP). ||CONVERSE_ITCH = (ITCH || CONVERSE) .
* sincronizzazione: MAKER = (make->ready->used->MAKER). USER = (ready->use->used->USER). ||MAKER_USER = (MAKER || USER).

####Proprietà della composizione parallela####
è commutativa e associativa: l'LTS di (A1||A2) è lo stesso di (A2||A1) e l'LTS di (A1||A2)||A3 è lo stesso di A1||(A2||A3). Possiamo quindi denotare la composizione parallela di multipli LTS A1, A2... An scrivendo A1 || A2 || ... || An

**Esempi di composizione parallela multipla e mutua esclusione pg. 54-57**

####Hiding####

Con P@{a1,...ax} si limita l'alfabeto di P alle azioni {a1..ax}. Altre azioni vengono trasformate in azione locale "tau": in questo modo si evita che tali azioni vengano sincronizzate con altri processi.

USER = (acquire->use->release->USER) @{acquire, release}

**Esempio mutua esclusione pg. 59-61**

###Reti di Petri###

Approccio grafico alla rappresentazione die sistemi concorrenti. Può essere considerato un'estensione naturale degli automi per sistemi concorrenti.

Gli stati sono detti "piazze" (place); più piazze sono contemporaneamente attive (contengono token).
Le transizioni modificano gli stati attivi:

* consumano un multi-insieme di stati attivi (token)
* generano un nuovo multi-insieme di stati attivi (token)

**Esempi pg. 63-74**

![](imgs/petridef.png)

####Marking graph####
E' un LTS che ne definisce il comportamento.

* **Configurazione** della PN (Marking): multi-insieme di piazze che indica la q.tà di token presenti in ogni piazza.
* **Passaggio di configurazione**: indica in che modo l'esecuzione di una transizione della rete di Petri modifica il marking corrente.
* **Configurazione iniziale**: marking iniziale M0.

A volte può essere *infinito* se la rete di Petri è unbounded (produce infiniti token).

**Esempi e def. formali 77, 78, 80**

####Model checking####

Su Petri nets “bounded” si verifica se una formula di LTL è soddisfatta come già visto:  
 
* Si considera il marking graph come un LTS che rappresenta il comportamento della rete di Petri e poi i modelli π di tutte le sue tracce massimali* Ora però proposizioni da stati dell’LTS: in questo caso gli stati sono i marking della PN. Sono definite proposizioni che consentono di osservare le piazze: proposizione “p” vera se e solo se piazza p contiene almeno un token
**Esempio: filosofi a cena pg. 82-83**