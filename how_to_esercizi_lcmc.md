#Linguaggi, compilatori e modelli computazionali

**Tipologie di esercizi**

##Linguaggi regolari

###Ricavare RE per un problema



###Dato un alfabeto, realizzare il DFA che riconosce un certo tipo di stringhe.

- Realizzare un DFA con grafico e/o tabella di transizione, ricordandosi che da uno stato di un DFA può uscire **una e una sola** transizione per un terminale (es. non possono esserci più di 1 freccia in uscita per "a").
- **Se viene richiesto l'automa minimo è necessario minimizzarlo tramite riempitabella (o, _solo se_ l'automa è semplice, ad occhio). _(matto)_**
- Se l'automa deve soddisfare più di una condizione (presenza operatori "or" o "and" nella consegna) allora basterà descrivere l'automa come unione dei due sottoautomi. Dati perciò due o più automi descritti tramite tuple $DFA_n = (Q_n, \{alphabet\}, \delta_n, q_{0n}, F_n)$, l'automa finale sarà:

$DFA_F = (Q = Q_1 \times Q_2 \times .... \times Q_n, $ 

${alphabet},$

$\delta = ((q_1...q_n)a) = (\delta(q_1, a), ... \delta(q_n,a)), ... , ((q_1...q_n)z) = (\delta(q_1, z), ... \delta(q_n,z)),$

$ q_{01..n}, $

$(q_1...q_n) \in F \Leftrightarrow q_1 \in F_1 \wedge  q_n \in F_n ) $

($\wedge$ = AND, $\vee$ = OR)

###Minimizzare il DFA tramite algoritmo riempitabella.
0. Creo una tabella diagonale di dimensione $(n-1) \times (n-1)$, con $n$ = numero di stati presenti nell'automa. Le colonne partono da indice 0, le righe da indice 1.
1. **1° step**: Segno con un **1** tutte le celle delle colonne e delle righe degli stati accettanti, **tranne** quelli dove la cella comprende due stati entrambi accettanti.
2. **2° step**: Valuto ogni cella e la segno con **2** se, per un determinato input, uno stato accetta e l'altro no (quindi se sono distinguibili).
3. **3° step**: Valuto le caselle vuote e guardo, per ogni input, gli stati in cui vanno a finire. Se la casella degli stati in cui vanno a finire è piena allora segno **3**, altrimenti la lascio bianca.

###Trasformare NFA in DFA

Creare tabella di transizione con gli stati che possiedono più di una freccia in uscita. I risultati per ogni nuovo stato saranno la somma degli stati descritti in input. Ad esempio se ho $\{p\}$ che va in $\{p,q\}$ con input 0, allora lo stato $\{p,q\}$ sarà dato dalla somma degli stati $p$ e $q$. E' possibile associare a ciascun nuovo stato una lettera così da migliorare la leggibilità. Gli stati finali del DFA saranno tutti quelli in cui compariranno i terminali finali del NFA. **Eliminare gli stati che non portano a nulla (nel nuovo DFA) ricorsivamente**. 

###Trasformare il NFA/DFA in RE tramite il metodo di eliminazione degli stati.

- Elimino uno stato per volta fino ad arrivare alla forma finale. Non si elimina mai lo stato iniziale. 
- Per ogni stato da eliminare cerco i suoi predecessori e i suoi successori. Calcolo poi per ogni coppia di predecessori/successori il cammino che esiste per andare da uno all'altro.
- Se ho un cammino passante per più stati lo concateno. Se ho un cammino alternativo tra i due stati li aggiungo con $+$ (es. $01+1$)
- La forma finale è:
   - se lo stato finale è quello iniziale, $E_q = R*$
   - se lo stato finale **NON** è quello iniziale, $E_q = (R + SU* T)*SU*$
   - se esiste più di uno stato finale ne tengo in considerazione uno per volta e li combino assieme alla fine: $E_{qf} = E_{q0} + E_{q1} + ... + E_{qn}$
   - cosa sono R, S, T, U? $R$ = autociclo sullo stato iniziale, $S$ = transizione iniziale $\rightarrow$ finale, $U$ = autociclo sullo stato finale, $T$ = transizione finale $\rightarrow$ iniziale.
   - **Attenzione**: se non c'è una componente con la stella di Kleene (es. se in $SU*T$ non ho $U*$) il resto rimane _invariato_, altrimenti _si annulla tutto_ (es. se in $SU*T$ non ho $T$).
 
###Trasformare $\epsilon$-NFA in DFA.
Creo un primo stato $q_0$ dove dentro ci metto tutti gli stati raggiungibili da $\epsilon$. Poi guardo se tra quegli stati ce n'è uno o più da cui parte un simbolo dell'alfabeto. In caso ci sia, creo un nuovo stato $q_1$ dove dentro ci metto la $\epsilon$-closure di quegli stati, ovvero tutti gli stati raggiungibili per ogni percorso la cui prima transazione sia il simbolo dell'alfabeto e poi ci sia $\epsilon$.

###Data una RE, trasformarla in NFA e/o DFA.
Sostituire $+$ (or), $\cdot$ (concatenazione) e $*$ (stella di Kleene) con i relativi sottoautomi a), b) e c) visibili nella figura. **Priorità degli operatori** in ordine: $*$, $\cdot$, $+$ (tenere conto delle parentesi!!).
	
![](imgs/t3-7-b.png)

##Linguaggi liberi

###Fornire una grammatica libera dal contesto (CFG) che rappresenta le stringhe di un linguaggio.

Rappresentare il linguaggio dato sotto forma di una grammatica libera dal contesto. Le grammatiche libere dal contesto sono costituite da una o più produzioni, le quali possono contenere caratteri terminali (lettere minuscole), caratteri rappresentativi di altre produzioni ("link", maiuscole) e $\epsilon$ (vuoto). Formato delle produzioni (a titolo esemplificativo): $S \rightarrow aB | B, B \rightarrow \epsilon$

###Dato un alfabeto, definire un automa a pila che riconosce un certo tipo di stringhe.

Un automa a pila permette di utilizzare una pila per tenere in memoria l'input letto ed utilizzarlo per effettuare o meno transizioni. All'inizio il PDA è vuoto, ovvero contiene in fondo allo stack $Z_0$. Ogni transizione di PDA è contraddistinta dalla tupla $(input, pop \rightarrow push) $, ovvero: si legge dall'input un carattere e, se corrisponde a $input$ e in cima allo stack è presente $pop$, quest'ultimo viene poppato e al suo posto viene pushato $push$. Ricordarsi che, mentre non sia possibile effettuare più pop contemporaneamente, è possibile pushare più di un simbolo per volta, ad esempio $(1, Z_0 \rightarrow 11Z_0)$

###Dire se una grammatica libera dal contesto è ambigua o meno.

Sia G = (V, T, P, S) una CFG. Diciamo che G e’ am- bigua se esiste una stringa in T∗ che ha piu’ di un albero sintattico.Se ogni stringa in L(G) ha un unico albero sintattico, G e’ detta non-ambigua.


##Classificazione e calcolabilità

###Dire se un linguaggio è regolare, libero, ricorsivo, ricorsivamente enumerabile, o nemmeno ric. enumerabile. Giustificare la risposta.

**Verificare se un linguaggio è regolare**: si applica il pumping lemma "standard". Condizioni del PL:

1. $y \neq \epsilon$ (y deve esistere)
2. $ |xy| \leq n$
3. $\forall k \geq 0, xy^kz \in L$

Si splitta il linguaggio in tre parti: $x$, $y$ e $z$, in cui $y$ può essere replicato $k$-volte. Per dimostrare se il linguaggio è regolare o meno è possibile fare esempi utilizzare gli esempi. La variabile $n$ presente nella definizione di PL può non essere la stessa dell'$n$ presente all'esponente nella consegna.
     
Analizzare tutti i casi possibili (es. "xy è di soli 0", "xy è di 0 e 1", "xy è solo 1"). Se esiste un modo per "pompare" il linguaggio, esso è regolare. Se non esiste un caso in cui il linguaggio sia regolare, esso non può essere regolare.
     
**Verificare se un linguaggio è libero**: si applica il pumping lemma per i CFL. Condizioni del CFL-PL:

1. $|vwx| \leq n$
2. $vx \neq \epsilon$
3. $\forall i \geq 0, uv^iwx^iy \in L$

Stesso procedimento del PL di base. Conclusione finale: se è possibile pompare il linguaggio, esso è libero dal contesto, altrimenti non lo è.

**Verificare se un linguaggio è ricorsivo, ricorsivamente enumerabile o nemmeno R.E.**: 

````

















````

##Algoritmi di parsing

###Data una grammatica, determinare se è di tipo LL(1)

Data una grammatica in input, creo la **tabella dei $First$ e dei $Follow$** dove nelle righe saranno presenti i simboli delle produzioni e nelle colonne saranno presenti i $First$ e i $Follow$ di ogni produzione.

- **$First$ è il primo simbolo di ogni produzione**. Se il simbolo è un terminale si inserisce il terminale, se il simbolo è una produzione si inseriscono i $First$ di quella produzione, se è $\epsilon$ si inserisce $\epsilon$. 
- **$Follow$ è il simbolo successivo alla produzione** all'interno dei risultati delle produzioni. Se ad esempio ho una produzione come $S \rightarrow aBc$, il follow della produzione $B$ è $c$. Anche qui se il $Follow$ è un terminale si inserisce il terminale, se è una produzione si inseriscono i $First$ di tale produzione, **DA VERIFICARE** se è l'insieme vuoto si inserisce il $Follow$ della produzione generatrice.

**Attenzione: se il $Follow$ va a finire su una produzione contenente $\epsilon$ come terminale, si passa al simbolo successivo nella produzione generatrice!**

Si va a creare quindi una seconda tabella, la **tabella di parsing**, che ha come righe le produzioni e come colonne i terminali (con colonna $\$$ in fondo).

Si analizzano tutte le produzioni, si considera il $First$ di ogni produzione e lo si inserisce nella tabella. Se una produzione ha $\epsilon$ nei $First$, si inserisce la produzione per i terminali specificati nel $Follow$ di tale produzione.

**Se in una cella è presente più di una produzione, la grammatica non è di tipo LL(1)**

###Data una grammatica, determinare se è di tipo SLR(1)

1. Cerchiamo i $First$ e $Follow$ come già facciamo per LL(1)
2. Aggiungiamo uno stato supplementare $S' \rightarrow S$
3. Disegnamo l'automa "con il pallino", ovvero con gli stati creati spostando il pallino ogni volta di una posizione verso destra.
4. Costruiamo la tabella avendo come righe gli stati dell’automa ($q_0 \rightarrow 0$, $q_1 \rightarrow 1$, ecc) e come colonne i simboli presenti nel RHS. 
5. Iniziamo a riempire la tabella con _accept_, _goto_, _shift_, _reduce_ in ordine. Partiamo da **accept**: si inserisce accept nello stato dell’automa in cui il linguaggio viene soddisfatto, ovvero la produzione artificiale $S' \rightarrow S$. _accept_ si inserisce nella colonna $.
6. Si inserisce **goto** in tabella per ogni transizione da uno stato all’altro che avviene grazie ad una produzione (simboli maiuscoli) 
7. Si inserisce **shift** per ogni transizione che avviene da uno stato all’altro tramite l’uso dei terminali (simboli minuscoli)
8. **reduce**: vado a guardare tutti gli stati finali di accettazione o, per dirla schietta, quelli in cui i pallini delle produzioni arrivano in fondo. In questi stati i percorsi finiscono e si “riducono” all’interno di tali produzioni. Non consideriamo la transizione $S' \rightarrow S$ visto che è già in accettazione.
I numeri che metteremo in tabella per la reduce non sono quelli degli stati come abbiamo visto finora, ma saranno le posizioni delle produzioni in questione nell’elenco numerato delle produzioni.

In caso di produzioni epsilon si inserisce solo il pallino (es. $A \rightarrow \cdot$) e non si fanno transizioni.

###Data una grammatica, determinare se è di tipo LR(1) e/o LALR(1)

####Automa LR(1) e parsing table
1. Si parte a disegnare il diagramma a stati come con SLR(1). Aggiungiamo lo stato supplementare $S' \rightarrow S$
2. **Per ogni produzione è necessario associare anche il look-ahead, da scrivere subito dopo (es. $S \rightarrow \cdot aX, a/b$)**.
3. Se la variabile presa in esame dal puntino $\cdot$ è una **produzione**, è necessario specificare anche le produzioni relative a quel simbolo. Il look-ahead di queste ultime sarà il $First$ delle stesse. 
	- Ad esempio, se dovessi avere come produzioni $S \rightarrow XX$, $X \rightarrow aX | b$, la relativa produzione di $S$ in $q_0$ sarà $S \rightarrow \cdot XX, \$ $, alla quale andranno aggiunte le produzioni raggiunte da $X$, ovvero $X \rightarrow aX | b$ (con look-ahead $First = {a,b}$)
4. Se il pallino $\cdot$ punta all'ultima produzione disponibile ma c'è un look-ahead, replico le stesse produzioni dello stato vecchio
5. Si continua poi come per SLR(1).
6. La tabella di parsing è uguale a SLR(1), con l'unica differenza che le **reduce** sono facilmente calcolabili poichè costituite dal look-ahead presente negli stati finali

####Automa LALR(1) e parsing table
1. Si prende l'automa LR(1) e si uniscono gli stati con produzioni uguali, lasciando perdere il look-ahead
2. Si costruisce la tabella di parsing tenendo conto che le **reduce** di stati uniti verranno combinate sullo stesso stato. (es. se $q_4$ ha la reduce in $a,b$ e $q_7$ ha la reduce in $\$$, allora la reduce di $q_{47}$ sarà in $a,b,\$$.


##Model checking

###Dati degli automi LTS, disegnarli assieme a una loro composizione. Dire quali delle formule LTL date soddisfano la composizione.

Disegnare gli automi tenendo conto che non esistono stati iniziali o finali. Dopodichè disegnare la composizione nella quale con molta probabilità saranno presenti variabili sovrapposte (interleaving) presenti in entrambi gli automi originari. Tali variabili fanno passare allo stato successivo **SOLO SE** eseguite insieme da tutti e due i sottoautomi. 

Le formule LTL sono composte da **<>** (sometimes), **()** (next), **[]** (everytime), **U** (until), **\/** (or), **/\** (and) - più le parentesi per dare precedenza agli operatori. Dire se nella composizione dell'automa finale le  formule sono soddisfatte o meno.

###Data una rete di Petri, specificare gli step (??)

````

















````